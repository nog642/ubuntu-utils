#!/root/.virtualenvs/aws_creds/bin/python
from base64 import urlsafe_b64encode
from getpass import getpass
import os
import secrets
import subprocess

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

ROOT = '/opt/aws_creds'


def secure_create(file_path, permissions):
    if os.path.exists(file_path):
        os.remove(file_path)
    return open(os.open(
        path=file_path,
        flags=os.O_EXCL | os.O_CREAT | os.O_WRONLY,
        mode=permissions
    ), 'wb')


def generate_salt():
    salt = secrets.token_bytes(32)
    with secure_create(ROOT + '/salt.dat', 0o400) as f:
        f.write(salt)
    return salt


def main():
    salt = generate_salt()
    password = getpass().encode()
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000
    )
    fernet = Fernet(urlsafe_b64encode(kdf.derive(password)))

    creds_fpath = ROOT + '/credentials.csv'
    with open(creds_fpath, 'rb') as f:
        creds_plaintext = f.read()
    creds_ciphertext = fernet.encrypt(creds_plaintext)
    with secure_create(ROOT + '/credentials.csv.encrypted', 0o400) as f:
        f.write(creds_ciphertext)
    subprocess.run(('srm', '-rvz', creds_fpath), check=True)


main()
