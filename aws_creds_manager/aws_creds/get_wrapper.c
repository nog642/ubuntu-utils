#include <stdio.h>
#include <unistd.h>

#define WRAPPED_PATH "/opt/aws_creds/get.py"

int main(int argc, char * argv[], char * envp[]) {
    execve(WRAPPED_PATH, (char * []){WRAPPED_PATH, NULL}, (char * []){NULL});
    perror("execve failed");
    return 1;
}
