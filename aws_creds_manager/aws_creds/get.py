#!/root/.virtualenvs/aws_creds/bin/python
from base64 import urlsafe_b64encode
import csv
from getpass import getpass
import io
import json
import os
import sys

from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

ROOT = '/opt/aws_creds'

TTY = io.TextIOWrapper(
    io.FileIO(os.open(b'/dev/tty', os.O_RDWR | os.O_NOCTTY), 'w+')
)


def get_unencrypted_creds():
    with open(ROOT + '/credentials.csv.encrypted', 'rb') as f:
        ciphertext = f.read()
    with open(ROOT + '/salt.dat', 'rb') as f:
        salt = f.read()
    while True:
        password = getpass('Password for {}/get: '.format(ROOT)).encode()
        fernet = Fernet(urlsafe_b64encode(PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000
        ).derive(password)))
        try:
            return fernet.decrypt(ciphertext)
        except InvalidToken:
            print('Invalid password.', file=TTY, flush=True)


def main():
    with io.StringIO(get_unencrypted_creds().decode(), newline='') as f:
        csv_f = csv.reader(f)
        next(csv_f)  # discard header

        # User name, Password, Access key ID, Secret access key, Console login link
        _, _, key_id, secret_key, _ = next(csv_f)
    print(json.dumps({
        'Version': 1,
        'AccessKeyId': key_id,
        'SecretAccessKey': secret_key
    }))


main()
