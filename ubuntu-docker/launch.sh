#!/bin/bash

name="$1"
subject="$2"

command="docker run -it"

if [ -z "$name" ]; then
    name=$(python3 -c 'import random;print("tmp_"+hex(random.randint(0x100000,0xffffff))[2:])')

    # delete on exit
    command+=" --rm"
fi
command+=" --name $name"

command+=" standard-ubuntu"

set -x
if [ -e "$subject" ]; then
    (sleep .5 && docker cp "$subject" $name:/home/user) &
fi
exec $command

# manual: docker run -it --name <name> standard-ubuntu
# manual: docker run -it --rm standard-ubuntu
# manual: docker run -it -p 8080:80 standard-ubuntu
