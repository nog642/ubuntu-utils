#!/bin/bash
set -x

# go to source directory
cd "$( dirname "${BASH_SOURCE[0]}" )"

command="docker build --tag standard-ubuntu"

if [ "$1" != "--keep" ]; then
    command+=" --rm"
fi

command+=" ."

$command

if [ "$1" != "--keep" ]; then
    docker image prune -f --filter label=stage=nog642-standard-ubuntu-installer
fi

# manual: docker build --rm --tag standard-ubuntu .
